package nl.utwente.di.translator;

public class Quoter {
    public double translateTemp(double temp) {
        return temp * 9.0 / 5.0 + 32.0;
    }
}
