package nl.utwente.di.translator;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Translator extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Quoter quoter;

    public void init() throws ServletException {
        quoter = new Quoter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Temperature Translator";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celcius: " +
                request.getParameter("temp") + "\n" +
                "  <P>Temperature in Farenheit: " +
                quoter.translateTemp(Double.parseDouble(request.getParameter("temp"))) +
                "</BODY></HTML>");
    }


}
